﻿namespace Betty.SlotMachine.CapitalHandlers
{
    public class CapitalHandler : ICapitalHandler
    {
        public decimal GetInitialBalance(decimal deposit, decimal newDeposit)
        {
            var twoDeposits = deposit + newDeposit;

            return twoDeposits;
        }

        public Capital Gamble(Capital capital, decimal stake)
        {
            if (stake < 1 || stake > 10)
            {
                Console.WriteLine($"Invalid stake! Please select bet between $1 and $10");
                return capital;
            }

            if (stake > capital.Deposit)
            {
                Console.WriteLine($"Not enough money! You cannot bet ${string.Format("{0:0.00}", stake)}, because your balance is: ${string.Format("{0:0.00}", capital.Deposit)}");
                return capital;
            }

            decimal expectedReturn = CalculateExpectedReturn(stake);
            var expectedLose = stake * 0.75m;

            if (expectedReturn <= expectedLose)
            {
                var win = capital.Deposit = capital.Deposit - stake;
                Console.WriteLine($"No luck this time! Your current balance is: ${string.Format("{0:0.00}", win)}");
                return capital;
            }
            else
            {
                capital.Deposit = capital.Deposit - stake + expectedReturn;
                Console.WriteLine($"Congrats - you won ${expectedReturn:F2} Your current balance is: ${string.Format("{0:0.00}", capital.Deposit)}");
                return capital;
            }
        }
        public Capital Withdraw(Capital capital, decimal withdraw)
        {
            if (withdraw > capital.Deposit)
            {
                Console.WriteLine("Invalid sum to withdraw.");

                return capital;
            }

            capital.Deposit = capital.Deposit - withdraw;
            Console.WriteLine($"Your withdraw of ${withdraw:F2} was successful. Your current balance is: ${capital.Deposit:F2}");
            return capital;
        }

        public decimal CalculateExpectedReturn(decimal betAmount)
        {
            decimal lossPercentage = 0.50m;
            decimal winningsUpTo2xPercentage = 0.40m;
            decimal winningsBetween2xAnd10xPercentage = 0.10m;

            decimal lossReturn = -betAmount;
            decimal winningsUpTo2xReturn = betAmount * 2;

            bool isWinningUpTo2x = (decimal)new Random().NextDouble() < winningsUpTo2xPercentage;

            decimal expectedReturn;

            if (isWinningUpTo2x)
            {
                expectedReturn = winningsUpTo2xReturn;
            }
            else
            {
                double randomMultiplier = new Random().NextDouble() * 8 + 2;
                decimal winningsBetween2xAnd10xReturn = betAmount * (decimal)randomMultiplier;
                expectedReturn = winningsBetween2xAnd10xReturn;
            }

            expectedReturn = (lossPercentage * lossReturn) +
                             (winningsUpTo2xPercentage * expectedReturn) +
                             (winningsBetween2xAnd10xPercentage * expectedReturn);
            return expectedReturn;
        }
    }
}

