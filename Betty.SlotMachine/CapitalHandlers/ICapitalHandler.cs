﻿namespace Betty.SlotMachine.CapitalHandlers
{
    public interface ICapitalHandler
    {
        decimal GetInitialBalance(decimal deposit, decimal newDeposit);

        Capital Gamble(Capital capital, decimal stake);

        Capital Withdraw(Capital capital, decimal withdraw);

        decimal CalculateExpectedReturn(decimal betAmount);
    }
}