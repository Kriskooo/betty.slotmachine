﻿namespace Betty.SlotMachine.CapitalHandlers
{
    public class Capital
    {
        public Capital(decimal deposit)
        {
            Deposit = deposit;
        }
        public decimal Deposit { get; set; }
    }
}
