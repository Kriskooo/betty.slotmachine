﻿using Betty.SlotMachine.CapitalHandlers;

namespace Betty.SlotMachine
{
    public static class SlotMachine
    {
        public static void Run()
        {
            var capitalHandler = new CapitalHandler();
            Console.WriteLine("Please, submit action:");
            var command = Console.ReadLine().Split(" ");
            var deposit = new Capital(0);
            while (command[0] != "exit")
            {
                switch (command[0])
                {
                    case "deposit":
                        deposit.Deposit = capitalHandler.GetInitialBalance(deposit.Deposit, decimal.Parse(command[1]));
                        Console.WriteLine($"Your deposit of ${decimal.Parse(command[1])} was successful. Your current balance is: ${string.Format("{0:0.00}", deposit.Deposit)}");
                        break;

                    case "bet":
                        capitalHandler.Gamble(deposit, decimal.Parse(command[1]));
                        break;

                    case "withdraw":
                        capitalHandler.Withdraw(deposit, decimal.Parse(command[1]));
                        break;
                }
                Console.WriteLine();
                Console.WriteLine("Please, submit action:");
                command = Console.ReadLine().Split(" ");
            }
            Console.WriteLine("Thank you for you playing! Hope to see you again soon.");
        }
    }
}
